/**
 * @interface ICountry
 * @property iso {string} - Country 2 letter ISO code
 * @property label {string} - Country Label
 */
export interface ICountry {
  iso: string,
  label: string,
}


interface IApp {

}


export default IApp;
