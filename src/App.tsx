import React, { Component } from 'react';
import './assets/scss/App.scss';
import IApp from "./interfaces/IApp";
import PollutionSearch from "./components/PollutionSearch/PollutionSearch";
import countries from "./config/countries";
import TCountry, {TCities} from "./types/general";
import openAQ from "./services/openAQ.service";
import wikipediaApi from "./services/wikipedia.service";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import ICity from "./components/City/ICity";
import City from "./components/City/City";
import ls from "local-storage";

type TAppState = {
  country: TCountry;
  cities: TCities;
  loading: boolean;
};

const initialState: TAppState = {
  country: ls.get('country') || undefined,
  cities: ls.get('cities') || [],
  loading: false,
};

class App extends Component<IApp, TAppState> {
  state:TAppState = initialState;

  completeSelect = (country: TCountry) => {
    this.setState({country, loading: true, cities: []},() => {
      ls.set('country', this.state.country);
    });
  };

  clearSelect = () => {
    this.setState({country: undefined, cities: []}, () => {
      ls.remove('country');
      ls.remove('cities');
    });
  };

  getCities = async(iso: string) => {
    const citiesData = await this.mapCities(await openAQ.getCitiesData(iso));
    if (this.state.loading) {
      this.setState({cities: citiesData, loading: false}, () => {
        ls.set('cities', this.state.cities);
      });
    }
  };

  getCityDescription = async(city: string) => {
    let cityData = await wikipediaApi.getCityDescription(city, this.state.country!.label);
    let {pages, pageids} = cityData.data.query;
    const pageId = pageids[0];
    return pages[pageId] ? pages[pageId].extract : 'No description Available';
  };

  mapCities = async (data:any[]): Promise<ICity[]> =>  {
    return await Promise.all(data.reduce((previousValue, currentValue) => {
      if (previousValue.indexOf(currentValue.city) === -1) previousValue.push(currentValue.city);
      return previousValue;
    },[])
      .slice(0,10)
      .map(async (city:string) => ({name: city, description: await this.getCityDescription(city)})));
  };

  renderCities = () => this.state.cities ?
    this.state.cities.map((city: ICity, index) =>
      <City key={index} index={index + 1} {...city}/>
    )
    : null;

  componentDidMount(){

  }

  componentDidUpdate(prevProps: Readonly<IApp>, prevState: Readonly<TAppState>): void {
    if (this.state.country !== undefined && prevState.country !== this.state.country) {
      this.getCities(this.state.country.iso);
    }
  }

  render() {
    return (
      <div className="app">
        <header className="app-header">
          <h1 className='app-name'><FontAwesomeIcon icon="wind" /> City Pollution</h1>
          <PollutionSearch
            value={this.state.country}
            completeOptions={countries}
            completeSelect={this.completeSelect}
            clearSelect={this.clearSelect}
          />
        </header>
        <main className='content'>
          <div className="cities-container">
            {this.renderCities()}
          </div>
          {
            this.state.loading &&
            <div className="loader">
                Loading Data ...
            </div>
          }
        </main>
      </div>
    );
  }
}

export default App;
