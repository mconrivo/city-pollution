import React from 'react';
import ReactDOM from 'react-dom';
import './assets/scss/index.scss';
import * as serviceWorker from './serviceWorker';
import App from "./App";
import { library } from '@fortawesome/fontawesome-svg-core'
import {faSearch, faWind, faChevronDown, faCloud, faArrowsAlt, faTimesCircle} from '@fortawesome/free-solid-svg-icons'

library.add(faSearch, faWind, faChevronDown, faCloud, faArrowsAlt, faTimesCircle);
ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
