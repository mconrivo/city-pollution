import {IRequestParams} from "../types/general";

const padDate = (str:number) => String(str).padStart(2, '0');

export const utcTodayRange = (today: Date = new Date()) => {
  const todayUTC  = `${today.getUTCFullYear()}-${padDate(today.getUTCMonth()+1)}-${padDate(today.getUTCDate())}`;
  return {from: `${todayUTC}T00:00:00`, to: `${todayUTC}T23:59:59`}
};

export const paramBuilder = (params: IRequestParams) => {
  return Object.keys(params).reduce((paramString, param) => {
    return params[param] ? paramString + `${param}=${params[param]}&` : paramString;
  }, '?').slice(0, -1);
};
