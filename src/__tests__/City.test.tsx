import * as React from 'react';
import {mount, shallow} from "enzyme";
import City from '../components/City/City';
import ICity from '../components/City/ICity'
const props:ICity = {
  name: 'Test City',
  description: 'description'
};

describe('City Component', () => {
  it('renders without crashing', () => {
    mount(<City {...props} />);
  });
});
