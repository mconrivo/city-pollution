import * as React from 'react';
import PollutionSearch from "../components/PollutionSearch/PollutionSearch";
import IPollutionSearch from "../components/PollutionSearch/IPollutionSearch";
import {mount, shallow} from 'enzyme';

const completeSelect = jest.fn();
const props:IPollutionSearch = {
  completeOptions:[{label:'Poland', iso: 'PL'}],
  completeSelect: completeSelect
};

describe('Pollution Search Component', () => {

  it('renders without crashing', () => {
    shallow(<PollutionSearch {...props} />);
  });

  describe('test value change', () => {
    const component = mount<PollutionSearch>(<PollutionSearch {...props} />);
    const input = component.find('.search-input');

    it('enters value', () => {
      input.simulate('change', { target: { value: 'pol' } });
      expect(component.state().optionsVisible).toBeTruthy();
    });

    it('selects value', () => {
      input.simulate('keyDown', {keyCode: 13});
      expect(completeSelect).toHaveBeenCalledWith({label:'Poland', iso: 'PL'});
    });
  })
});
