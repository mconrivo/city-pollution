import {ICountry} from "../interfaces/IApp";
import ICity from "../components/City/ICity";

type TCountry = ICountry | undefined;
export type TCities = ICity[] | undefined;
export type IRequestParams = {
  [name: string]: string | number | null | undefined;
}
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export default TCountry;
