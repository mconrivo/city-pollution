import axios from 'axios';
import {paramBuilder, utcTodayRange} from "../helpers/utils";

class OpenAQService {
  private openAQApiUrl: string;

  constructor() {
    this.openAQApiUrl = 'https://api.openaq.org/v1/'
  }

  async getCitiesData (country:string) {
    const {from, to} = utcTodayRange();
    const requestFields = {
      country,
      order_by: 'value',
      parameter: 'pm25',
      date_from: from,
      date_to: to,
      sort: 'desc',
      limit: 1000,
    };
    let res = await axios.get(this.openAQApiUrl + `measurements${paramBuilder(requestFields)}`);
    return res.data.results
  }
}

const openAQ = new OpenAQService();
export default openAQ;
