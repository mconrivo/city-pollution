import axios, {AxiosInstance} from 'axios';
import {paramBuilder} from "../helpers/utils";

class WikipediaApi {
  private wikipediaApiUrl: string;
  private instance: AxiosInstance;

  constructor() {
    this.instance = axios.create();
    this.instance.interceptors.request.use((config) => {
      config.url = `${config.url}&origin=*`;
      // Do something before request is sent
      return config;
    }, function (error) {
      // Do something with request error
      return Promise.reject(error);
    });
    this.wikipediaApiUrl = 'https://en.wikipedia.org/w/api.php'
  }

  async getCityDescription (city:string, country:string) {
    const requestFields = {
      format: 'json',
      action: 'query',
      prop: 'extracts',
      exintro: 'true',
      indexpageids: 'true',
      converttitles: 'en',
      generator: 'search',
      gsrsearch: `${city}+city+${country}`,
      gsrlimit: 1
    };
    return await this.instance.get(this.wikipediaApiUrl + paramBuilder(requestFields))
  }
}

const wikipediaApi = new WikipediaApi();
export default wikipediaApi;
