import {ICountry} from "../interfaces/IApp";

var countries: ICountry[] = [
  {
    iso: 'PL',
    label: 'Poland'
  },
  {
    iso: 'DE',
    label: 'Germany'
  },
  {
    iso: 'ES',
    label: 'Spain'
  },
  {
    iso: 'FR',
    label: 'France'
  }
];
export default countries;
