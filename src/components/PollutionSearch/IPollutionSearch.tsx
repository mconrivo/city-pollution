import {ICountry} from "../../interfaces/IApp";
import TCountry from "../../types/general";

interface IPollutionSearch {
  completeOptions: ICountry[];
  completeSelect: (option: TCountry) => void;
  clearSelect: () => void;
  value: TCountry;
}

export default IPollutionSearch;
