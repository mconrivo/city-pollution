import * as React from 'react';
import IPollutionSearch from './IPollutionSearch';
import '../../assets/scss/PollutionSearch.scss';
import {ICountry} from "../../interfaces/IApp";
import TCountry, {Omit} from "../../types/general";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

type TPollutionSearch = {
    optionsVisible: boolean;
    selectedOption: number;
    filteredOptions: ICountry[],
    userInput: string;
    userOption: TCountry;
    inputElement: React.RefObject<HTMLInputElement>;
};

const initalState: Omit<TPollutionSearch,'inputElement'> = {
    optionsVisible: false,
    selectedOption: 0,
    filteredOptions: [],
    userInput: '',
    userOption: undefined,
};

export default class PollutionSearch extends React.Component<IPollutionSearch, TPollutionSearch> {
    state: TPollutionSearch = {
        ...initalState,
        userInput: this.props.value? this.props.value.label : '',
        userOption: this.props.value,
        inputElement: React.createRef(),
    };

    renderAutoComplete = () => {
        return this.state.optionsVisible ?
          <ul className="autocomplete-options">
              {this.renderOptions()}
          </ul>
          : null;
    };

    renderOptions = () => {
        const {filteredOptions, selectedOption} = this.state;
        const options = filteredOptions.map((option, index) => {
            let className ='autocomplete-option' ;
            if (index === selectedOption) {
                className += ' option-active';
            }

            return (
              <li
                className={className}
                key={option.iso}
                onClick={() => this.onOptionClick(option)}
              >
                  {option.label}
              </li>
            );
        });
        return options.length > 0 ? options : <li className="autocomplete-option">-- No Options -- </li>;
    };

    onOptionClick = (option: ICountry) => {
        this.setState({
            selectedOption: 0,
            filteredOptions: [],
            optionsVisible: false,
            userInput: option.label,
            userOption: option
        }, () => {
            this.props.completeSelect(this.state.userOption)
        });
    };

    onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        const { selectedOption, filteredOptions } = this.state;
        if (e.keyCode === 13) {

            this.setState({
                selectedOption: 0,
                optionsVisible: false,
                userInput: filteredOptions[selectedOption].label,
                userOption: filteredOptions[selectedOption]
            }, () => {
                this.state.inputElement.current!.blur();
                this.props.completeSelect(this.state.userOption)
            });
        }
        else if (e.keyCode === 38) {
            const option = selectedOption === 0 ? filteredOptions.length - 1 : selectedOption - 1;
            this.setState({ selectedOption: option });
        }
        else if (e.keyCode === 40) {
            const option = selectedOption === filteredOptions.length - 1 ? 0 : selectedOption + 1;
            this.setState({ selectedOption: option });
        }
    };

    onChange = (e: React.ChangeEvent<HTMLInputElement>) =>{
        const userInput = e.target.value;
        const {completeOptions} = this.props;
        const filteredOptions = completeOptions.filter(
          option =>
            option.label.toLowerCase().indexOf(userInput.toLowerCase()) > -1
        );
        this.setState({
            userInput: userInput,
            optionsVisible: userInput.length > 0,
            filteredOptions
        });
    };

    clearInput = () => {
      this.setState({...initalState}, () => {
          this.props.clearSelect();
      });
    };

    componentDidMount() {
        if (this.props.value) {
            this.setState({userInput: this.props.value.label, userOption: this.props.value})
        }
    }

    render() {
        const {userInput} = this.state;
        return (
          <div className="search-container">
              <input
                ref={this.state.inputElement}
                className="search-input"
                type="text"
                placeholder="Enter Country Name"
                value={userInput}
                onKeyDown={this.onKeyDown}
                onChange={this.onChange}
              />
              {
                  userInput.length > 0 &&
                  <div className="search-clear" onClick={this.clearInput}>
                      <FontAwesomeIcon icon={'times-circle'}/>
                  </div>
              }
              {this.renderAutoComplete()}
          </div>
        );
    };
}
