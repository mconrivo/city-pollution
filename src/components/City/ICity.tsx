/**
 * @interface ICity
 * @property name {string} - Name of the city
 * @property description {string} - City description from wikipedia
 */
interface ICity {
  name: string,
  description: string,
  index?: number,
}

export default ICity;
