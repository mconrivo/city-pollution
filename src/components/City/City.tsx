import * as React from 'react';
import ICity from './ICity';
import '../../assets/scss/City.scss';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


type TCity = {
    opened: boolean;
    descriptionElement: React.RefObject<HTMLDivElement>;
    maxHeight: number;
};

export default class City extends React.Component<ICity, TCity> {
    state: TCity = {
        opened: false,
        descriptionElement: React.createRef(),
        maxHeight: 0
    };


    calculateMaxHeight = () => {
        const {descriptionElement} = this.state;
        this.setState({maxHeight: (descriptionElement.current!.clientHeight + 100)});
    };

    toggleDescription = () => {
          this.setState({opened: !this.state.opened})
    };

    componentDidMount() {
        this.calculateMaxHeight();
        window.addEventListener('resize', this.calculateMaxHeight)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.calculateMaxHeight);
    }

    render() {
        const {description, name, index} = this.props;
        const {opened} = this.state;
        return (
          <div
            className={['city', opened ? 'opened' : 'closed'].join(' ')}
            style={{maxHeight: opened ? this.state.maxHeight : undefined}} >
              <div className="city-header" onClick={this.toggleDescription}>
                  <div className="city-name">
                      {index && `${index}. `}{name}
                  </div>
                  <FontAwesomeIcon
                    icon="chevron-down"
                    className={['description-icon', opened ? 'up' : ''].join(' ')}
                  />
              </div>
              {
                  description &&
                  <div
                    ref={this.state.descriptionElement}
                    className={['description-container'].join(' ')}
                    dangerouslySetInnerHTML={{__html: description }}
                  />
              }
          </div>
        )
    };
}
